'use strict';

module.exports = [
  './node_modules/jquery/dist/jquery.min.js',
  './node_modules/swiper/js/swiper.min.js',
  './node_modules/select2/dist/js/select2.js',
  './node_modules/lity/dist/lity.js',
  './node_modules/aos/dist/aos.js',
  './node_modules/svg4everybody/dist/svg4everybody.js'
];
