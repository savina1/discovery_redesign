$(document).ready(function () {

  //tags tabs
  //search bar
  //hide search on click anywhere besides the search
  //initialize channels slider
  //initialize show-cards slider
  //initialize premiers slider
  //initialize day slider
  //initialize top slider
  //animations
  //position footer to bottom of the page
  //toggle mobile menu
  //hide mobile menu on click anywhere besides the menu
  //toggle dropdown menu on down-arrow
  //remove border from the last item in dropdown menu
  //select plugin
  //ajax load videos
  //ajax load topics
  //ajax load discovery videos
  //ajax load recommended shows
  //ajax load timetable
  //accordion schedule page
  //letters on shows page


    // this will get the full URL at the address bar
    // var url = window.location.href;

    // var url = window.location.href,
    //   parts = url.split("/"),
    //   second_part = parts[3];
    // console.log(second_part);
    //
    // // passes on every "a" tag
    // $(".nav__link").each(function() {
    //   // checks if its the same on the address bar
    //   if (second_part == this.href) {
    //     console.log(this.href);
    //     $(this).closest(".nav__item").addClass("active");
    //     //for making parent of submenu active
    //     // $(this).closest("li").parent().parent().addClass("active");
    //   }
    // });



  svg4everybody({});


  if ($(window).width() < 481) {
    $('.hero__background_desktop').css({'background-image': 'none'});
  }

  //tags tabs
  function initTabs() {
    const triggers = $('[data-tab-trigger]');
    const scenes = $('[data-tab-scene]');

    triggers.on('click', function (e) {

      e.preventDefault();
      // triggers.removeClass('active');
      // $(this).toggleClass('active');

      const thisTriggerValue = $(this).data('tab-trigger');
      const thisScene = $(`[data-tab-scene=${thisTriggerValue}]`);
      if (!$(this).hasClass('active')) {
        triggers.removeClass('active');
        $(this).addClass('active');
        // $(`[data-tab-trigger="${thisTriggerValue}"]`).addClass('active');
        scenes.removeClass('active');
        thisScene.addClass('active');
      }
    });
  }

  initTabs();


//search bar
  $('.search__icon').on('click', function (e) {
    $('.header__nav').toggleClass('hidden');
    $('.header__promo').toggleClass('hidden');
    $('.actions__mobile-menu').toggleClass('hidden');
    $('.search__button').toggleClass('active');
    $('.search__input').toggleClass('active');
    $('.search__icon').toggleClass('active');
    $('.search').toggleClass('active');

    if ($(window).width() < 595) {
      $('.header__logo').toggleClass('hidden');
    }

  });


  //hide search on click anywhere besides the search
  $(document).on('click', function (e) {
    if (!$(e.target).hasClass('actions__search') && !(e.target).closest('.actions__search')) {
      $('.header__nav').removeClass('hidden');
      $('.header__promo').removeClass('hidden');
      $('.actions__mobile-menu').removeClass('hidden');
      $('.search__button').removeClass('active');
      $('.search__input').removeClass('active');
      $('.search__icon').removeClass('active');
      $('.search').removeClass('active');

      if ($(window).width() < 595) {
        $('.header__logo').removeClass('hidden');
      }

    }
  });


//initialize channels slider
  var swiperOptions6 = {
    // Optional parameters
    direction: 'horizontal',
    navigation: {
      nextEl: '.other-channels-swiper-button-next',
      prevEl: '.other-channels-swiper-button-prev',
    },
    breakpoints: {
      // when window width is >= 316px
      316: {
        slidesPerView: 2
      },

      532: {
        slidesPerView: 4
      },
      710: {
        slidesPerView: 5
      },
      1217: {
        slidesPerView: 6
      }
    },
    spaceBetween: 30,
    mousewheelControl: true,
    keyboardControl: true,
  };

  var swiperChannels = new Swiper('.other-channels__wrapper', swiperOptions6);


  //initialize show-cards slider
  var swiperOptions3 = {
    // Optional parameters
    direction: 'horizontal',
    navigation: {
      nextEl: '.videos-swiper-button-next',
      prevEl: '.videos-swiper-button-prev',
    },
    breakpoints: {
      // when window width is >= 316px
      316: {
        slidesPerView: 1
      },

      532: {
        slidesPerView: 2
      },
      710: {
        slidesPerView: 3
      },
      1217: {
        slidesPerView: 4
      }
    },
    spaceBetween: 30,
    mousewheelControl: true,
    keyboardControl: true,
  };

  var swiperVideos = new Swiper('.videos__wrapper', swiperOptions3);


  //initialize premiers slider
  var swiperOptions4 = {
    // Optional parameters
    direction: 'horizontal',
    navigation: {
      nextEl: '.premier-swiper-button-next',
      prevEl: '.premier-swiper-button-prev',
    },
    breakpoints: {
      // when window width is >= 316px
      316: {
        slidesPerView: 1
      },
      532: {
        slidesPerView: 2
      },
      900: {
        slidesPerView: 3
      },
      1217: {
        slidesPerView: 3
      }
    },
    spaceBetween: 30,
    mousewheelControl: true,
    keyboardControl: true,
  };

  var swiperPremier = new Swiper('.premier__wrapper', swiperOptions4);


  //initialize day slider
  if ($('.day-slider').length) {

    var swiperOptions7 = {
      // Optional parameters
      direction: 'horizontal',
      navigation: {
        nextEl: '.days-swiper-button-next',
        prevEl: '.days-swiper-button-prev',
      },
      breakpoints: {
        // when window width is >= 316px
        316: {
          slidesPerView: 2
        },
        532: {
          slidesPerView: 3
        },
        900: {
          slidesPerView: 4
        },

        1217: {
          slidesPerView: 5
        },
        1430: {
          slidesPerView: 6
        }
      },
      // slidesPerView: 5,
      // centeredSlides: true,
      // touchRatio: 0,
      mousewheelControl: true,
      keyboardControl: true,
      on: {},
      initialSlide: $('.day-slider__item.active').length ? $('.day-slider__item.active').index() : 0

    };

    var swiperDays = new Swiper('.day-slider__box', swiperOptions7);

    $('.day-slider__content').on('click', function (e) {
        let url = $(this).data('url');
        let date = $(this).data('date');

        $(this).closest('.day-slider__item').addClass('active').siblings().removeClass('active');
        // window.location.href = `${url}?date=${date}`;
      }
    );

  }


  //initialize top slider
  if ($('.main-hero').length) {

    var interleaveOffset = 0.5;
    // function initSlider() {
    // const mySwiper = new Swiper('.swiper-container', {


    var swiperOptions1 = {
      // Optional parameters
      direction: 'horizontal',
      parallax: true,
      loop: true,
      // effect: 'fade',
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: 'true',
      },
      speed: 1000,
      autoplay: {
        delay: 8000,
      },
      watchSlidesProgress: true,
      mousewheelControl: true,
      keyboardControl: true,
      on: {
        progress: function () {
          var swiper = this;
          for (var i = 0; i < swiper.slides.length; i++) {
            var slideProgress = swiper.slides[i].progress;
            var innerOffset = swiper.width * interleaveOffset;
            var innerTranslate = slideProgress * innerOffset;
            // var backgroundPosition = swiper.slides[i].backgroundPosition;
            // console.log(backgroundPosition)

            swiper.slides[i].querySelector('.slider-top__background').style.transform =
              'translate3d(' + innerTranslate + 'px, 0, 0)';

            // swiper.slides[i].querySelector('.slider-top__background').style.backgroundPositionX = backgroundPosition - 12 + 'px';


            // swiper.slides[i].querySelector('.slider-top__background').style.backgroundSize =
            //   'auto 120%';

            // swiper.slides[i].querySelector('.slider-top__background').classList.add('animation-bigger');
          }
        },
        touchStart: function () {
          var swiper = this;
          for (var i = 0; i < swiper.slides.length; i++) {
            swiper.slides[i].style.transition = '';
            // swiper.slides[i].querySelector('.slider-top__background').style.backgroundSize =
            //   'auto 100%';
          }
        },
        setTransition: function (speed) {
          var swiper = this;
          for (var i = 0; i < swiper.slides.length; i++) {
            swiper.slides[i].style.transition = speed + 'ms';
            swiper.slides[i].querySelector('.slider-top__background').style.transition =
              speed + 'ms';
          }
        },
        // transitionEnd: function () {
        //   console.log('это я')
        //   var swiper = this;
        //   for (var i = 0; i < swiper.slides.length; i++) {
        //     swiper.slides[i].querySelector('.slider-top__background').classList.add('animation-bigger');
        //   }
        // }
      }

    };

    // });
    // }

    var swiper = new Swiper('.main-hero__container', swiperOptions1);

    $('[data-lity]').on('click', function (e) {
      swiper.autoplay.stop();
    })
  }

  //animations
  AOS.init({
    duration: 1000,
    once: true
  });


  //position footer to bottom of the page
  function footerToBottom() {
    const footer = $('.footer'),
      wrapper = $('.wrapper'),
      footerHeight = footer.innerHeight();
    footer.css({
      marginTop: -footerHeight
    });
    wrapper.css({
      paddingBottom: footerHeight
    });

  }

  footerToBottom();

  //toggle mobile menu
  $('.actions__mobile-menu').on('click', function (e) {
    $('.mobile-menu').toggleClass('active');
    $('.header__nav').toggleClass('active');
    $('.header__promo_mobile').toggleClass('active');
  });

  //hide mobile menu on click anywhere besides the menu
  $(document).on('click', function (e) {
    if (!$(e.target).hasClass('actions__mobile-menu') && !(e.target).closest('.actions__mobile-menu')) {
      $('.mobile-menu').removeClass('active');
      $('.header__nav').removeClass('active');
      $('.header__promo_mobile').removeClass('active');
    }
  });
  $('.header__nav').on('click', function (e) {
    e.stopPropagation();
  });


  //toggle dropdown menu on down-arrow
  var dropdownIsActive = false;
  $('.nav__open-dropdown').on('click', function (e) {
    if (!dropdownIsActive) {
      $('.dropdown').slideToggle('slow', function () {
        // animation complete
        dropdownIsActive = false;
      });
      $('.nav__open-dropdown').toggleClass('active');
      dropdownIsActive = true;
    }
  });

  //remove border from the last item in dropdown menu
  if ($(window).width() > 767) {
    var lastDropdownItem = $('.nav__list  li:visible:last');
    lastDropdownItem.css({
      'border-bottom': 'none'
    });
  }

  // select plugin
  $('.select__select2').select2({
    placeholder: 'Введите ваш регион',
    theme: 'opaque',
    width: '100%',
    'language': {
      'noResults': function () {
        return 'Совпадений не найдено';
      }
    }
  });


  //ajax load videos
  function loadContent() {
    let delay = 0;
    const container = $('.video-sect__small-videos-list');
    const contentArr = [{
      videoLink: '//www.youtube.com/watch?v=oNVpp5haQac',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Прив'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Очень много текста очень'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'овоаоараарра аарара раОРВГП НАП ГР'
    }];
    contentArr.forEach((el) => {
      let elem = $('<div/>', {class: 'video-sect__video-item'});
      elem.html(`
        <div class="show-card animation-fade-in">
          <a href="${el.videoLink}" data-lity class="show-card__link">
            <div class="show-card__cover">
                <img src="${el.imgLink}" class="show-card__pic" alt="">
                <div class="show-card__play"><svg width="90" height="90" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="1" y="1" width="88" height="88" rx="44" stroke="#fff" stroke-width="2"/><path fill-rule="evenodd" clip-rule="evenodd" d="M54 45L39 33v24l15-12z" fill="#C54214"/></svg></div>
            </div>
            <div class="show-card__info">
                <div class="show-card__name title title_h4 animated">${el.title}</div>
            </div>
          </a>
        </div>
          
        `);
      // setTimeout(() => {
      container.append(elem);
      // }, delay += 130);

    });
  }

  $('[data-js-ajax]').on('click', function (e) {
    // console.log($(this));
    $(this).addClass('hidden');
    // $(this).addClass('hidden');
    e.preventDefault();
    loadContent();
    setTimeout(() => {
      // $(this).removeClass('hidden');
      $(this).removeClass('hidden');
    }, 900);

  });

  //ajax load topics
  function loadArticles() {
    let delay = 0;
    const container = $('.tab-scenes__item.active .articles__list');
    const contentArr = [{
      articleLink: '#',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Прив'
    }, {
      articleLink: '#',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Очень много текста очень'
    }, {
      articleLink: '#',
      imgLink: './assets/img/video-cover.jpg',
      title: 'овоаоараарра аарара раОРВГП НАП ГР'
    },
      {
        articleLink: '#',
        imgLink: './assets/img/video-cover.jpg',
        title: 'овоаоараарра аарара раОРВГП НАП ГР'
      }];
    contentArr.forEach((el) => {
      let elem = $('<div/>', {class: 'articles__item'});
      elem.html(`
        <div class="show-card animation-fade-in">
          <a href="${el.articleLink}" class="show-card__link">
            <div class="show-card__cover">
                <img src="${el.imgLink}" class="show-card__pic" alt="">
            </div>
            <div class="show-card__info">
                <div class="show-card__name title title_h4 animated">${el.title}</div>
            </div>
            <div class="show-card__more"> Подробнее
                <span class="show-card__more-arrow animated">
                <svg viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 1L6 6L1 1" stroke="#81D1EA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
                </span>
            </div>
          </a>
        </div>
       
        `);
      // setTimeout(() => {
      container.append(elem);
      // }, delay += 130);

    });
  }

  $('[data-js-ajax-articles]').on('click', function (e) {
    // console.log($(this));
    $(this).addClass('hidden');
    // $(this).addClass('hidden');
    e.preventDefault();
    loadArticles();
    setTimeout(() => {
      // $(this).removeClass('hidden');
      $(this).removeClass('hidden');
    }, 900);

    setTimeout(() => {
      $('.show-card').removeClass('animation-fade-in');
    }, 3500)

  });



//ajax load discovery videos
  function loadDiscoveries() {
    let delay = 0;
    const container = $('.open-world');
    const contentArr = [{
      videoLink: '//www.youtube.com/watch?v=oNVpp5haQac',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Прив'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Очень много текста очень'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'овоаоараарра аарара раОРВГП НАП ГР'
    },
      {
        videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
        imgLink: './assets/img/video-cover.jpg',
        title: 'овоаоараарра аарара раОРВГП НАП ГР'
      }];
    contentArr.forEach((el) => {
      let elem = $('<div/>', {class: 'interesting__item interesting__item_four-col'});
      elem.html(`
        <div class="show-card animation-fade-in">
          <a href="${el.videoLink}" data-lity class="show-card__link">
            <div class="show-card__cover">
                <img src="${el.imgLink}" class="show-card__pic" alt="">
                <div class="show-card__play"><svg width="90" height="90" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="1" y="1" width="88" height="88" rx="44" stroke="#fff" stroke-width="2"/><path fill-rule="evenodd" clip-rule="evenodd" d="M54 45L39 33v24l15-12z" fill="#C54214"/></svg></div>
            </div>
            <div class="show-card__info">
                <div class="show-card__name title title_h4 animated">${el.title}</div>
            </div>
          </a>
        </div>
          
        `);
      // setTimeout(() => {
      container.append(elem);
      // }, delay += 130);

    });
  }

  $('[data-js-ajax-discoveries]').on('click', function (e) {
    // console.log($(this));
    $(this).addClass('hidden');
    e.preventDefault();
    loadDiscoveries();
    setTimeout(() => {
      $(this).removeClass('hidden');
    }, 900);

  });


  //ajax load recommended shows
  function loadShows() {
    let delay = 0;
    const container = $('.recommended__list');
    const contentArr = [{
      videoLink: '//www.youtube.com/watch?v=oNVpp5haQac',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Прив',
      season: 'с 9 февраля по субботам в 12.00'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'Очень много текста очень',
      season: 'с 9 июня по средам в 15.00'
    }, {
      videoLink: '//www.youtube.com/watch?v=x3-00ErCfEY',
      imgLink: './assets/img/video-cover.jpg',
      title: 'овоаоараарра аарара раОРВГП НАП ГР',
      season: 'со 2 февраля по субботам в 11.00'
    }];
    contentArr.forEach((el) => {
      let elem = $('<div/>', {class: 'recommended__item'});
      elem.html(`
        <div class="show-card animation-fade-in">
          <a href="${el.videoLink}" class="show-card__link">
            <div class="show-card__cover">
                <img src="${el.imgLink}" class="show-card__pic" alt="">
            </div>
            <div class="show-card__info">
                <div class="show-card__name title title_h4 animated">${el.title}</div>
                <div class="show-card__season show-card__season_lowercase animated">${el.season}</div>
            </div>
          </a>
        </div>
          
        `);
      // setTimeout(() => {
      container.append(elem);
      // }, delay += 130);

    });
  }

  $('[data-js-ajax-recommended]').on('click', function (e) {
    $(this).addClass('hidden');
    e.preventDefault();
    loadShows();
    setTimeout(() => {
      $(this).removeClass('hidden');
    }, 900);

  });


  //ajax load timetable
  function loadTimetable() {
    return new Promise((resolve, reject) => {
      let delay = 0;
      // const container = $('.shows-list__shows-accordion');
      let tempContainer = $('<ul/>', {class: 'shows-list__shows-accordion accordion'});


      setTimeout(() => {
        const contentArr = [{
          time: '12:43',
          title: 'Прив',
          season: 'ето 5 сезончик',
          text_p1: 'Ве&nbsp;те, кто интересуется вопросом, как возникло космическое пространство и&nbsp;что оно сбой представляет, а&nbsp;также заботиться вопросами Вселенной и&nbsp;всего остального, что нас окружает, обязательно оценят этот документальный фильм из&nbsp;нескольких серий, который способен ответить на&nbsp;многие вопросы мироздания.',
          text_p2: 'текст текст еще текст',
          imgLink: './assets/img/main-hero-mobile.jpg',
          isInPrime: true
        }, {
          time: '15:44',
          title: 'Прив',
          season: 'ето 6 сезончик',
          text_p1: 'пространство и&nbsp;что оно сбой представляет, а&nbsp;также заботиться вопросами Вселенной и&nbsp;всего остального, что нас окружает, обязательно оценят этот документальный фильм из&nbsp;нескольких серий, который способен ответить на&nbsp;многие вопросы мироздания.',
          text_p2: 'текст',
          imgLink: './assets/img/main-hero-mobile.jpg'
        }, {
          time: '19:02',
          title: 'хоба',
          season: 'ето 7 сезончик',
          text_p1: 'Ве&nbsp;те, кто интересуется вопросом, как возникло космическое пространство и&nbsp;что оно сбой представляет, а&nbsp;также заботиться вопросами Вселенной и&nbsp;всего остального, что нас окружает, обязательно оценят этот документальный фильм из&nbsp;нескольких серий, который способен ответить на&nbsp;многие вопросы мироздания.',
          text_p2: 'текст',
          imgLink: './assets/img/main-hero-mobile.jpg'
        }];
        contentArr.forEach((el) => {
          let elem = $('<li/>', {class: 'accordion__block show-cell'});
          elem.html(`
                        
						<div class="accordion__action show-cell__heading">
							<div class="show-cell__left">
								<div class="show-cell__time">${el.time}</div>
								<div class="show-cell__prime">${el.isInPrime ? 'В эфире' : '' }</div>
							</div>
							<div class="show-cell__right">
								<div class="show-cell__title">${el.title}</div>
								<div class="show-cell__season">${el.season}</div>
							</div>
							<div class="show-cell__trigger">
								<svg viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 1L6 6L1 1" stroke="#81D1EA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
							</div>
						</div>

						<div class="accordion__sliding-part">
							<div class="show-about">
								<div class="show-about__wrapper">
									<div class="show-about__text">
										<div class="show-about__p">${el.text_p1}</div>
										<div class="show-about__p">${el.text_p2}</div>
										<div class="show-about__button-container">
											<div class="show-about__button button button_bordered">
												<span class="button__text">Перейти к шоу</span>
											</div>
										</div>
									</div>
									<div class="show-about__pic">
										<img src="${el.imgLink}" alt="" class="show-about__img">		
									</div>
								</div>
							</div>
						</div>
					
          
        `);


          tempContainer.append(elem);


        });
        resolve(tempContainer);

      }, 50);

    });


  }


  $('[data-js-ajax-timetable]').on('click', function (e) {
    $('.preloader').fadeIn(300);

    const container = $('.shows-list__wrapper-for-list');
    loadTimetable().then(res => {
      container.html('');
      container.append(res);
      // console.log(123);
      $('.preloader').fadeOut(300);
    });


  });


  //accordion schedule page
  (function ($) {
    var tSlide = 250;
    var isAnimated = false;


    var $arrows = $('.show-cell__trigger');

    let wiWidth = $(window).width();

    $(document).on('click', '.accordion__action', function () {
      const $allPanels = $('.accordion__sliding-part');
      const $parent = $(this).parent();
      const $target = $(this).next('.accordion__sliding-part');
      const $arrow = $(this).children('.show-cell__trigger');
      // console.log($(this));

      if (!isAnimated) {
        isAnimated = true;
        if (wiWidth < 481) {
          $parent.toggleClass('active');
          $target.slideToggle();
          $arrow.toggleClass('active');
          // console.log('if');
          isAnimated = false;
        } else {
          // console.log('else');
          if (!$parent.hasClass('active')) {
            // console.log('inner-if');
            $allPanels.parent().removeClass('active');
            // console.log($allPanels);
            $allPanels.slideUp(tSlide, function () {
              isAnimated = false;
            });

            // $allPanels.hide(0,  () => {
            //   isAnimated = false;
            //   const offsetTop = $(this).offset().top;
            //   $("html, body").animate({ scrollTop: offsetTop}, 200);
            // });


            $arrows.removeClass('active');


            $parent.addClass('active');
            $arrow.addClass('active');
            $target.slideDown(tSlide);

          } else {
            // console.log('inner-else');
            $parent.removeClass('active');
            $arrow.removeClass('active');
            $target.slideUp(tSlide, function () {
              isAnimated = false;
            });

            // $allPanels.hide(0,  () => {
            //   isAnimated = false;
            //   const offsetTop = $(this).offset().top;
            //   $("html, body").animate({ scrollTop: offsetTop}, 200);
            // });
          }
        }


      }
    });

  })(jQuery);


  //letters on shows page

  if ($('.hero_shows').length) {
    // Cache selectors
    var lastId,
      topMenu = $('.letters'),
      // wrapperTopMenu = $('.letters'),
      topMenuHeight = topMenu.outerHeight() + 1,
      distanceFromTop = topMenu.offset().top,

      // All list items
      menuItems = topMenu.find('a'),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function () {
        var item = $($(this).attr('href'));
        if (item.length) {
          return item;
        }
      });

// Bind click handler to menu items
// so we can get a fancy scroll animation
    menuItems.click(function (e) {
      var href = $(this).attr('href'),
        offsetTop = href === '#' ? 0 : $(href).offset().top - topMenuHeight + 1;
      $('html, body').stop().animate({
        scrollTop: offsetTop
      }, 850);
      e.preventDefault();
    });

// Bind to scroll
    $(window).on('resize', function () {
      distanceFromTop = topMenu.offset().top;
      if ($(window).scrollTop() >= distanceFromTop) {
        topMenu.addClass('active');
      } else {
        topMenu.removeClass('active');
      }
    });
    $(window).scroll(function () {
      if ($(window).scrollTop() >= distanceFromTop) {
        topMenu.addClass('active');
      } else {
        topMenu.removeClass('active');
      }


      // Get container scroll position (px from top to change the letter color)
      var fromTop = $(this).scrollTop() + topMenuHeight + 50;

      // Get id of current scroll item
      var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop)
          return this;
      });
      // Get the id of the current element
      cur = cur[cur.length - 1];
      var id = cur && cur.length ? cur[0].id : '';

      if (lastId !== id) {
        lastId = id;
        // Set/remove active class
        menuItems
          .parent().removeClass('active')
          .end().filter('[href="#' + id + '"]').parent().addClass('active');
      }
    });

  }

});
